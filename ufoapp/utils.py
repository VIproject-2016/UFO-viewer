import dateutil.parser
import datetime, json

def parseDate(dt):
    if dt is None or dt == "":
        return None
    try:
        return dateutil.parser.parse( dt )
    except Exception:
        return None


def docToJson(docList):
    return json.dumps( docList, default=_datetime_handler )


def _datetime_handler(x):
    if isinstance( x, datetime.datetime ):
        return x.isoformat( )
    else:
        raise TypeError( x )
