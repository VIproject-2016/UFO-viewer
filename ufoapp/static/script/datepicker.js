$('#reportrange').daterangepicker({
    "showDropdowns": true,
    "alwaysShowCalendars": true,
    "ranges": {
        "2000 to now": [
            "2000-01-01",
            "2016-12-31"
        ],
        "2010 to now": [
            "2010-01-01",
            "2016-12-31"
        ],
        "Last year": [
            "2016-01-01",
            "2016-12-31"
        ],
        "Last 6 months": [
            "2016-06-01",
            "2016-12-31"
        ],
        "Last month": [
            "2016-12-01",
            "2016-12-31"
        ]
    },
    "locale": {
        "format": "YYYY-MM-DD",
        "separator": " - ",
        "applyLabel": "Apply",
        "cancelLabel": "Cancel",
        "fromLabel": "From",
        "toLabel": "To",
        "customRangeLabel": "Custom",
        "weekLabel": "W",
        "daysOfWeek": [
            "Su",
            "Mo",
            "Tu",
            "We",
            "Th",
            "Fr",
            "Sa"
        ],
        "monthNames": [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ],
        "firstDay": 1
    },
    "startDate": "2016-12-01",
    "endDate": "2016-12-08",
    "minDate": "2000-01-01",
    "maxDate": "2016-12-31"
}, function(start, end, label) {
    console.log("New date range selected: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
});