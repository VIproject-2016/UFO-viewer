var symbol = {
    'triangle' : [[0, 0], [10, 0], [5, 10], [0, 0]],
    'disk' : [[4, 0], [6, 0], [8, 1], [9, 2], [10, 4], [10, 6], [9, 8], [8, 9], [6, 10], [4, 10], [2, 9], [1, 8], [0, 6], [0, 4], [1, 2], [2, 1], [4, 0], [4, 6], [6, 6], [6, 4], [4, 4], [4, 0]],
    'square' : [[0, 0], [10, 0], [10, 10], [0, 10], [0, 0]],
    'light' : [[5, 0], [6, 3], [7, 3], [7, 4], [10, 5], [7, 6], [7, 7], [6, 7], [5, 10], [4, 7], [3, 7], [3, 6], [0, 5], [3, 4], [3, 3], [4, 3], [0, 0]],
    'fireball' : [[1, 0], [5, 0], [6, 1], [10, 9], [10, 10], [9, 10], [1, 6], [0, 5], [0, 1], [1, 0]],
    'oval' : [[4, 0], [6, 0], [7, 1], [8, 3], [8, 7], [7, 9], [6, 10], [4, 10], [3, 9], [2, 7], [2, 3], [3, 1], [4, 0]],
    'egg' : [[4, 0], [6, 0], [7, 1], [8, 3], [8, 5], [7, 8], [5, 10], [3, 8], [2, 5], [2, 3], [3, 1], [4, 0]],
    'cross' : [[4, 0], [6, 0], [6, 4], [10, 4], [10, 6], [6, 6], [6, 10], [4, 10], [4, 6], [0, 6], [0, 4], [4, 4], [4, 0]],
    'rectangle' : [[3, 0], [7, 0], [7, 10], [3, 10], [3, 0]],
    'circle' : [[4, 0], [6, 0], [8, 1], [9, 2], [10, 4], [10, 6], [9, 8], [8, 9], [6, 10], [4, 10], [2, 9], [1, 8], [0, 6], [0, 4], [1, 2], [2, 1], [4, 0]],
    'sphere' : [[4, 0], [6, 0], [8, 1], [9, 2], [10, 4], [10, 6], [9, 8], [9, 6], [8, 5], [7, 5], [6, 7], [6, 8], [7, 9], [8, 9], [6, 10], [4, 10], [2, 9], [1, 8], [0, 6], [0, 4], [1, 2], [2, 1], [4, 0]],
    'diamond' : [[5, 0], [8, 5], [5, 10], [2, 5], [5, 0]],
    'cigar' : [[0, 4], [5, 3], [10, 4], [10, 6], [5, 7], [0, 6], [0, 4]],
    'teardrop' : [[4, 0], [6, 0], [8, 1], [9, 3], [9, 4], [8, 6], [5, 0], [2, 6], [1, 4], [1, 3], [2, 1], [4, 0]],
    'cylinder' : [[4, 0], [6, 0], [8, 1], [8, 9], [6, 10], [4, 10], [2, 9], [2, 1], [4, 0]]
};

var clustersColor = '#727994';
var singleReportsColor = '#91002D';

var styleCache = {};

function styleFunction(feature) {
    var shape = feature.get('shape');
    var style = styleCache[shape];
    if (!style) {
        var fsymbol;
        if (shape.toLowerCase() in symbol) {
            fsymbol = shape.toLowerCase();
        } else {
            fsymbol = 'unknown';
        }
        style = new ol.style.Style({
            image: new ol.style.Icon({
                src: "/static/images/symbol-" + fsymbol + ".png",
                color: singleReportsColor
            })
        });
        styleCache[shape] = style;
    }
    return style;
}


function toFeature(el) {
    var loc = el['geoinfo']['geometry']['location'];
    var dateString = moment(el['date']).format("DD/MM/YYYY HH:mm");
    var result = new ol.Feature({
        geometry: new ol.geom.Point(ol.proj.fromLonLat([loc['lng'] + (Math.random() * .1), loc['lat'] + (Math.random() *0.1)])),
        text: el['geoinfo']['address'] + ", " + dateString,
        address: el['geoinfo']['address'],
        date: dateString,
        shape: el['shape'],
        link: el['reportlink'],
        summary: el['summary']
    });
    return result;
}

var googleLayer = new olgm.layer.Google();
var projection = new ol.proj.Projection({
    code: 'black',
    units: 'pixels',
    extent: [0, 0, 1024, 968]
});
var blackLayer = new ol.layer.Image({
    source: new ol.source.ImageStatic({
        attributions: '',
        url: '/static/images/black.png',
        imageExtent: [0, 0, 1024, 968],
        projection: projection
    }),
    opacity: 1
});
var myFeatures;
var source;
var clusterSource;
var clusters;
var map;
var olGM;

var info = $('#info');
var dLocation = $('#reportDetails-location');
var dTime = $('#reportDetails-time');
var dSummary = $('#reportDetails-summary');
var dLink = $('#reportDetails-link');
var reportDetails = $('#reportDetails');

info.tooltip({
    animation: false,
    trigger: 'manual'
});

var displayFeatureInfo = function(pixel) {
    info.css({
        left: (pixel[0] + 14) + 'px',
        top: (pixel[1] - 10) + 'px'
    });
    var feature = map.forEachFeatureAtPixel(pixel, function(feature) {
        return feature;
    });
    if (feature) {
        if (feature.get('features').length == 1) {
            $('#map').css( 'cursor', 'pointer' );
            info
                .attr('data-original-title', feature.get('features')[0].get('text'))
                .tooltip('show');
        } else {
            $('#map').css( 'cursor', 'default' );
            info
                .attr('data-original-title', feature.get('features').length + ' occurences')
                .tooltip('show');
        }
    } else {
        info.tooltip('hide');
        $('#map').css( 'cursor', 'default' );
    }
};

var displayFeatureReportDetails = function(pixel) {
    var feature = map.forEachFeatureAtPixel(pixel, function(feature) {
        return feature;
    });
    if (feature) {
        if (feature.get('features').length == 1) {
            var report = feature.get('features')[0];
            reportDetails.html(
                '<div><span class="tag tag-info">where</span><span>' + report.get('address') + ', ' + report.get('date') + '</span></div>' +
                '<div><span class="tag tag-info">summary</span><span>' + report.get('summary') + '</span></div>' +
                '<div><span class="tag tag-info">link</span><span><a href="' + report.get('link') + '" target="_blank">' + report.get('link') + '</a></span></div>');
        } else {
            info
                .attr('data-original-title', feature.get('features').length + ' occurences')
                .tooltip('show');
            reportDetails.html('This cluster contains <strong>' + feature.get('features').length + '</strong> occurences. Zoom more to see individual reports.');
        }
    } else {
        reportDetails.text('Click on a report to see its details here.');
    }
};

var clusterStyle = function(feature) {
    if (feature) {
        var size = feature.get('features').length;
        var style;
        if (size == 1) {
            style = styleFunction(feature.get('features')[0])
        } else {
            style = styleCache[size];
            if (!style) {
                style = new ol.style.Style({
                    image: new ol.style.Circle({
                        radius: Math.min(15, 9 + (size / 5)),
                        stroke: new ol.style.Stroke({
                            color: '#fff'
                        }),
                        fill: new ol.style.Fill({
                            color: clustersColor
                        })
                    }),
                    text: new ol.style.Text({
                        text: size.toString(),
                        fill: new ol.style.Fill({
                            color: '#fff'
                        })
                    })
                });
                styleCache[size] = style;
            }
        }
        return style;
    }
};

function changeMapData(content) {
    myFeatures = content.map(toFeature);

    source = new ol.source.Vector({
        features: myFeatures
    });

    clusterSource = new ol.source.Cluster({
        distance: 55,
        source: source
    });

    clusters = new ol.layer.Vector({
        source: clusterSource,
        style: clusterStyle
    });

    if (map == null) {
        newView = new ol.View({
            projection: "EPSG:3857",
            center: [-6273944.401820901, 4966745.199457582], //[2316354.584980343, -5678181.107649204],
            zoom: 3
        })
    } else {
        newView = map.getView();
    }

    $('#map').empty();

    map = new ol.Map({
        target: 'map',
        interactions: olgm.interaction.defaults(),
        layers: [
            googleLayer, clusters
        ],
        view: newView
    });

    map.on('pointermove', function(evt) {
        if (evt.dragging) {
            info.tooltip('hide');
            return;
        }
        displayFeatureInfo(map.getEventPixel(evt.originalEvent));
    });

    map.on('click', function(evt) {
        displayFeatureInfo(evt.pixel);
        displayFeatureReportDetails(evt.pixel);
    });

    olGM = new olgm.OLGoogleMaps({map: map}); // map is the ol.Map instance
    try {
        olGM.activate();
    }catch(e){

    }
}