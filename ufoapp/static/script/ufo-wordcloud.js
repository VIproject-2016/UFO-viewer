/**
 * inspired from http://bl.ocks.org/jwhitfieldseed/9697914
 * @author: Lucy Linder
 * @date: 13.11.2016
 */
// Encapsulate the word cloud functionality
function wordCloud(selector, options) {
    options = options || {};
    options.key = options.key || "text";
    options.value = options.value || "size";
    var w = options.w || 1000, h = options.height || 600;
    var fill = d3.scale.category20();

    //Construct the word cloud's SVG element
    var svg = d3.select(selector).append("svg")
        .attr( "viewBox", "0 0 " + w + " " + h )
        .append("g")
        .attr( "transform", "translate(" + [ w >> 1, h >> 1] + ")" );

    var onClickFunc = options.onClick;

    //Draw the word cloud
    function draw(words) {
        var cloud = svg.selectAll("g text")
            .data(words, function(d) { return d[options.key]; });

        //Entering words
        cloud.enter()
            .append("text")
            .style("font-family", "Impact")
            .style("fill", function(d, i) { return fill(i); })
            .attr("text-anchor", "middle")
            .attr('font-size', function( d ){ return d.size + "px"; } )
            .text(function(d) { return d[options.key]; })
            .on( 'click', function( d ){ if(onClickFunc) onClickFunc(d); } );

        //Entering and existing words
        cloud
            .transition()
            .duration(600)
            .style("font-size", function(d) { return d.size + "px"; })
            .attr("transform", function(d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
            })
            .style("fill-opacity", 1);

        //Exiting words
        cloud.exit()
            .transition()
            .duration(200)
            .style('fill-opacity', 1e-6)
            .attr('font-size', 1)

            .remove();


        if(options.tooltip){
            d3.selectAll("title").remove(); // clear previous titles
            cloud
                .append("svg:title")
                .text(options.tooltip);
        }
    }

    // http://bl.ocks.org/aaizemberg/78bd3dade9593896a59d
    // https://github.com/d3/d3-3.x-api-reference/blob/master/Ordinal-Scales.md
    var c10 = d3.scale.category10();
    var c20 = d3.scale.category20();
    var c20b = d3.scale.category20b();
    var c20c = d3.scale.category20c();

    var fills = [c10, c20, c20b, c20c];
    var fillIdx = 0;
    //Use the module pattern to encapsulate the visualisation code. We'll
    // expose only the parts that need to be public.
    return {

        generate: function(words) {
            fill = fills[fillIdx];
            fillIdx = (fillIdx+1) % fills.length;
            fontSize = d3.scale['log']().range( [10, 100] ); // in sqrt, log, linear
            if( words.length ){
                fontSize.domain( [+words[words.length - 1][options.value] || 1, +words[0][options.value] ] );
            }

            d3.layout.cloud().size([w, h])
                .words(words)
                .rotate(function() { return (Math.random() < 0.5 ? -1 : 1) * ~~(Math.random() * 3) * 30; })
                .font("Impact")
                .spiral('archimedean')
                .fontSize(function(d) { return  fontSize( +d[options.value] ); })
                .text(function(d){return d[options.key] })
                .on("end", draw)
                .start();
        }
    }

}
