Array.prototype.rotate = function (n) {
    return this.slice(n, this.length).concat(this.slice(0, n));
};

function changeGraphData(data) {
    var days = {};
    var hours = [
        ['0:00 - 1:00', 0],
        ['1:00 - 2:00', 0],
        ['2:00 - 3:00', 0],
        ['3:00 - 4:00', 0],
        ['4:00 - 5:00', 0],
        ['5:00 - 6:00', 0],
        ['6:00 - 7:00', 0],
        ['7:00 - 8:00', 0],
        ['8:00 - 9:00', 0],
        ['9:00 - 10:00', 0],
        ['10:00 - 11:00', 0],
        ['11:00 - 12:00', 0],
        ['12:00 - 13:00', 0],
        ['13:00 - 14:00', 0],
        ['14:00 - 15:00', 0],
        ['15:00 - 16:00', 0],
        ['16:00 - 17:00', 0],
        ['17:00 - 18:00', 0],
        ['18:00 - 19:00', 0],
        ['19:00 - 20:00', 0],
        ['20:00 - 21:00', 0],
        ['21:00 - 22:00', 0],
        ['22:00 - 23:00', 0],
        ['23:00 - 0:00', 0]
    ];


    data.map(function (o) {
        var d = new Date(o.date);
        if (o.hours) hours[d.getHours()][1]++;
        d.setHours(0, 0, 0);
        var time = d.getTime();
        if (time in days) {
            days[time][1]++;
        } else {
            days[time] = [time, 1];
        }
    });

    var result = Object.values(days);
    hours = hours.rotate(12);
    hours.push(hours[0]);

    Highcharts.stockChart('chart1', {

        credits: {
            enabled: false
        },
        chart: {
            plotBackgroundColor: "#F8F8F8",
        },
        rangeSelector: {
            enabled: false
        },

        scrollbar: {
            enabled: false
        },

        title: {
            text: 'Reports distribution by hours of the day'
        },

        navigator: {
            enabled: false
        },

        xAxis: [{
            labels: {
                formatter: function () {
                    return hours[this.value][0].split(":")[0]; // + ":30";
                }
            }
        }],

        yAxis: {
            labels: {
                enabled: false
            }

        },

        series: [{
            name: 'Reports',
            data: hours,
            tooltip: {
                valueDecimals: 0
            }
        }]
    });

    Highcharts.stockChart('chart2', {

        credits: {
            enabled: false
        },
        chart: {
            plotBackgroundColor: "#F8F8F8"
        },

        navigator: {
            //maskFill: 'rgba(255, 0, 0, 1)',
        },

        rangeSelector: {
            selected: 1,
            allButtonsEnabled: true,
            inputEnabled: true,
            inputDateFormat: "%d/%m/%Y",
            inputEditDateFormat: "%d/%m/%Y"
        },

        title: {
            text: 'Reports by date'
        },

        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
                month: '%m/%Y',
                year: '%Y'
            }
        },

        series: [{
            name: 'Reports',
            data: result,
            tooltip: {
                valueDecimals: 0
            }
        }]
    });
}
