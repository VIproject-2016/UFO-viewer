(function () {

    angular
        .module("ufo.rest", ['ngResource'])
        .constant('baseUrl', 'http://localhost:5000/api')
        .factory('RestService', RestService);


    function RestService($resource, baseUrl) {
        return $resource('', {}, {


            /**
             * @ngdoc
             * @name reports
             * @methodOf ufo.rest.RestService
             *
             * @description
             * Get reports between from and to dates (as query parameters).
             *
             * @returns {httpPromise} resolves with the object {stats: <object>, results: <array>}, or fails with error
             * description.
             */
            reports: {
                method: 'GET',
                url: baseUrl + '/reports',
                isArray: false
            },


            /**
             * @ngdoc
             * @name frequencies
             * @methodOf ufo.rest.RestService
             *
             * @description
             * Get word count frequencies between from and to dates (as query parameters).
             * The optional shapes=<shape[,shape]> can be used to filter the shapes (default to all).
             *
             * @returns {httpPromise} resolves with the object{"stats": <stats>, "results": <results>}, or fails with error
             * description.
             */
            frequencies: {
                method: 'GET',
                url: baseUrl + '/frequencies',
                isArray: true
            },

            /**
             * @ngdoc
             * @name stats
             * @methodOf ufo.rest.RestService
             *
             * @description
             * Get some statistics (count and count per shape) for the given period (from and to query params).
             *
             * @returns {httpPromise} resolves with the object{"stats": <stats>}, or fails with error
             * description.
             */
            reportsStats: {
                method: 'GET',
                url: baseUrl + '/reports/stats',
                isArray: false
            },

            /**
             * @ngdoc
             * @name stats
             * @methodOf ufo.rest.RestService
             *
             * @description
             * Get some statistics (count and count per shape) for the given period (from and to query params).
             *
             * @returns {httpPromise} resolves with the object{"stats": <stats>}, or fails with error
             * description.
             */
            reportsStats: {
                method: 'GET',
                url: baseUrl + '/reports/stats',
                isArray: false
            }

        });
    }

})();

