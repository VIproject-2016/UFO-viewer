(function () {
    var app = angular.module("ufo", [
        'ufo.rest',
        'ngSanitize', 'ngCsv'
    ]);

    app.directive('btTooltip', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                $(element).attr('title', scope.$eval(attrs.title));
                $(element).tooltip({trigger: attrs.trigger || 'hover'});
                $(element).bind('click', function () {
                    $(this).tooltip('show');
                });
            }
        };
    });

    app.directive('btButton', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function ($scope, element, attr, ctrl) {
                // init
                ctrl.$setViewValue(attr.ngModel);
                var label = element.parent("label");
                // set/unset 'active' class when model changes
                $scope.$watch(attr.ngModel, function (newValue, oldValue) {
                    label.toggleClass('active', ctrl.$viewValue);
                    $(element).attr('checked', newValue);
                });

                // update model when button is clicked
                label.bind('click', function (e) {

                    // avoid multiple fires
                    /*if(e.srcElement.tagName == "INPUT") {
                        $scope.$apply(function (scope) {
                            ctrl.$setViewValue(!ctrl.$viewValue);
                        });
                    }*/

                    // don't let Bootstrap.js catch this event,
                    // as we are overriding its data-toggle behavior.
                    e.stopPropagation();
                });
            }
        };
    });

    app.directive('daterangePicker', function () {
        return {
            restrict: 'E',
            require: 'ngModel',
            template: '<input class="form-control" type="text" name="datefilter" />',
            translude: true,
            link: function (scope, element, attrs, ngModel) {
                $(element).find("input").daterangepicker({
                    "showDropdowns": true,
                    "ranges": {
                        "2000 to now": [
                            "01/01/2000",
                            "12/31/2016"
                        ],
                        "2010 to now": [
                            "01/01/2010",
                            "12/31/2016"
                        ],
                        "Last year": [
                            "01/01/2016",
                            "12/31/2016"
                        ],
                        "Last 6 months": [
                            "01/06/2016",
                            "12/31/2016"
                        ],
                        "Last month": [
                            "01/12/2016",
                            "12/31/2016"
                        ]
                    },
                    "locale": {
                        "format": "MM/DD/YYYY",
                        "separator": " - ",
                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",
                        "fromLabel": "From",
                        "toLabel": "To",
                        "customRangeLabel": "Custom",
                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
                    },
                    "startDate": "12/01/2016",
                    "endDate": "12/08/2016",
                    "minDate": "01/01/2000",
                    "maxDate": "12/31/2016"
                }, function (start, end, label) {
                    ngModel.$setViewValue({from: new Date(start), to: new Date(end) });
                    scope.$apply();
                });
            }
        }
    });

})();