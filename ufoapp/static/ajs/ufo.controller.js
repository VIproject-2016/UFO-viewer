(function () {
    angular.module("ufo")
        .controller("MainCtrl", MainCtrl);

    function MainCtrl(RestService) {

        var self = this;
        self.shapes_count = {};
        self.stats = {};
        self.dates = {start: "", end: ""};
        self.cloud = null;
        self.shapesChanged = false;
        self.shapesActive = [];
        self.getCSV = reportsToCSV;

        self.getTooltip = function (name, shape) {
            return name + " " + (shape.active ? "(enabled)" : "(disabled)");
        };

        self.shapes = {
            "triangle": {
                "active": true,
                "img": "/static/images/symbol-triangle.png"
            },
            "disk": {
                "active": true,
                "img": "/static/images/symbol-disk.png"
            },
            "square": {
                "active": true,
                "img": "/static/images/symbol-square.png"
            },
            "light": {
                "active": true,
                "img": "/static/images/symbol-light.png"
            },
            "fireball": {
                "active": true,
                "img": "/static/images/symbol-fireball.png"
            },
            "oval": {
                "active": true,
                "img": "/static/images/symbol-oval.png"
            },
            "egg": {
                "active": true,
                "img": "/static/images/symbol-egg.png"
            },
            "cross": {
                "active": true,
                "img": "/static/images/symbol-cross.png"
            },
            "rectangle": {
                "active": true,
                "img": "/static/images/symbol-rectangle.png"
            },
            "circle": {
                "active": true,
                "img": "/static/images/symbol-circle.png"
            },
            "sphere": {
                "active": true,
                "img": "/static/images/symbol-sphere.png"
            },
            "diamond": {
                "active": true,
                "img": "/static/images/symbol-diamond.png"
            },
            "cigar": {
                "active": true,
                "img": "/static/images/symbol-cigar.png"
            },
            "teardrop": {
                "active": true,
                "img": "/static/images/symbol-teardrop.png"
            },
            "cylinder": {
                "active": true,
                "img": "/static/images/symbol-cylinder.png"
            }
        };

        self.onDateChange = onDateChange;
        self.onShapeChange = onShapeChange;
        self.onShapeApply = onShapeApply;
        self.onResetShapes = onResetShapes;

        _init();


        // ==========================================


        function _init() {
            self.dates = {from: new Date("2016-01-01"), to: new Date("2016-08-02")};
            onDateChange();
            $('.nav.navbar-nav').show();
        }

        function onDateChange() {
            toggleOverlay(true);
            RestService.reports(self.dates, applyNewDates);
            getFreqs();
        }

        function applyNewDates(response) {
            self.shapes_count = response.stats.shapes_count;
            self.stats = response.stats;
            self.reports = response.results;
            setNewData();
            toggleOverlay(false);
        }

        function onShapeChange() {
            self.shapesChanged = false;
            angular.forEach(self.shapes, function (s, n) {
                var idx = self.shapesActive.indexOf(n);
                if ((idx == -1 && s.active) || (idx >= 0 && !s.active)) {
                    self.shapesChanged = true;
                    return false;
                }
            });
        }

        function onResetShapes() {
            angular.forEach(self.shapes, function (s, n) {
                s.active = self.shapesActive.indexOf(n) >= 0;
            });
            self.shapesChanged = false;
        }

        function onShapeApply() {
            getFreqs();
            setNewData();
        }

        function setNewData() {
            var filtered = [];

            // update active filters
            self.shapesActive = [];
            angular.forEach(self.shapes, function (s, n) {
                if(s.active) self.shapesActive.push(n);
            });
            self.shapesChanged = false;

            angular.forEach(self.reports, function (r) {
                if (self.shapes[r.shape] && self.shapes[r.shape].active) {
                    filtered.push(r);
                }
            });
            changeMapData(filtered);
            changeGraphData(filtered);
        }


        function getFreqs() {
            var shapes = [];
            angular.forEach(self.shapes, function (s, key) {
                if (s.active) shapes.push(key);
            });

            if (shapes.length)
                RestService.frequencies({
                    from: self.dates.from,
                    to: self.dates.to,
                    shapes: shapes.join(",")
                }, generateCloud);
        }

        function generateCloud(response) {
            console.log("freqs", response);
            if (!self.cloud) {
                self.cloud = wordCloud("#cloud",
                    {key: "_id", value: "count", tooltip: tooltipContent}); //, onClick: displayInfos});
            }
            console.log("generating cloud");
            self.cloud.generate(response);

        }

        function tooltipContent(d) {
            return "count: " + d.count + ", docs: " + d.docs;
        }

        function toggleOverlay(show) {
            if (show) $('#waiting-overlay').fadeIn();
            else $('#waiting-overlay').fadeOut();
        }

        function reportsToCSV() {
            self.csvHeaders = "date, shape, link, lat, long, address, summary".split(", ");

            var csv = self.reports.map(function (o) {
                var oproj = {};
                oproj.date = o.date;
                oproj.shape = o.shape;
                oproj.link = o.reportlink;
                oproj.lat = o.geoinfo.geometry.location.lat;
                oproj.long = o.geoinfo.geometry.location.lng;
                oproj.address = o.geoinfo.address;
                oproj.summary = o.summary;
                return oproj;
            });
            console.log(csv);
            return csv;
        }

    }
})();