import pymongo
from utils import *
from bson import Code

# another way to deal with results from mongo is to use:
#   from bson import json_util
#   jsonOptions = json_util.JSONOptions( datetime_representation=json_util.DatetimeRepresentation.ISO8601 )
#   cursor = db.find(...)
#   jstring = json_util.dumps( cursor, json_options=jsonOptions )
# The only problem is that date fields are encoded : "date": {"$date": "2013-01-01T23:00:00.000Z"}
# Using the basic json, we do one more iteration, but we get:  "date": "2013-01-01T23:00:00"

# client = pymongo.MongoClient('scyllarus.protectator.ch', 27017)
client = pymongo.MongoClient('localhost', 27017)
db = client.nuforc.events
#fdb = client.nuforc.freqs  # collection storing the results of the map reduce

SUMMARY_KEYS = {"_id": 1, "date": 1, "hours": 1, "shape": 1, "summary": 1, "reportlink": 1, "geoinfo.address": 1,
                "geoinfo.geometry.location": 1}

FREQ_MAX_WORDS = 100
# used for map_reduce in mongo
fMap = Code("function(){for(var key in this.frequencies){emit(key, {cnt:this.frequencies[key],docs:1});}}")
fReduce = Code(
    "function(k,v){var cnt=0,docs=0;for(var i=0;i<v.length;i++){cnt+=v[i].cnt;docs+=v[i].docs;}return{cnt:cnt,docs:docs};}")


# fFinalize = Code("function(k,v){ return {word: k, cnt: v.cnt, docs: v.docs};}")


def frequencies1(dfrom, dto, shapes=[]):
    """
    return the top recurrent words in records
    :param dfrom: date from to select records (datetime object)
    :param dto: date to to select records (datetime objects)
    :param shapes: an optional list of shapes to retain
    :return: the top frequencies, as well as the statistics of the map reduce.
     Format {"stats": <stats>, "results": <results>}, with
      stats being {"emit":<cnt>, "input":<cnt>, "output":<cnt>, "reduce":<cnt> } and
      results an array of {"_id": <word>, "value": {"cnt": <count>, "docs": <count> } }
    """
    conditions = {
        "date": {"$gte": dfrom, "$lt": dto},
        "frequencies": {"$exists": True}}
    if shapes:
        conditions["shape"] = {"$in": shapes}

    # map_reduce stores the result inside a collection (3rd param=name of the output coll)
    stats = db.map_reduce(fMap, fReduce, "freqs", query=conditions, full_response=True)  # , finalize=fFinalize)
    # retrieve the top 100 words from the newly created collection "freqs"
    result = fdb.find({}).sort("value.cnt", -1).limit(FREQ_MAX_WORDS)
    return {"stats": stats["counts"], "results": list(result)}


def frequencies(dfrom, dto, shapes=[]):
    """
    return the top recurrent words in records
    :param dfrom: date from to select records (datetime object)
    :param dto: date to to select records (datetime objects)
    :param shapes: an optional list of shapes to retain
    :return: the top frequencies, as well as the statistics of the map reduce.
     Format {"stats": <stats>, "results": <results>}, with
      stats being {"emit":<cnt>, "input":<cnt>, "output":<cnt>, "reduce":<cnt> } and
      results an array of {"_id": <word>, "value": {"cnt": <count>, "docs": <count> } }
    """
    conditions = {
        "date": {"$gte": dfrom, "$lt": dto},
        "freqArray": {"$exists": True}}
    if shapes:
        conditions["shape"] = {"$in": shapes}
    print(conditions)
    results = db.aggregate([
        {"$match": conditions},
        {"$unwind": "$freqArray"},
        {"$group":
            {
                "_id": '$freqArray.word',
                "count": {"$sum": "$freqArray.cnt"},
                "docs": {"$sum": 1}
            }
        },
        {"$sort": {"count": -1}},
        {"$limit": 100}
    ])

    return list(results)


def reports(dfrom, dto, stats_only=False):
    """
    return a list of records, skipping some fields such as reporttext.
    :param dfrom: date from to select records (datetime object)
    :param dto: date to to select records (datetime objects)
    :param stats_only: if set to true, return statistics only
    :return: {"stats": <stats>, "results": <list of docs> }, with stats being
        {"all": <count>, "complete": <count>", "shapes_count": { <shape>: <count> }}
    """
    cnt = db.find({"date": {"$gte": dfrom, "$lt": dto}}).count()
    cursor = db.find({
        "date": {"$gte": dfrom, "$lt": dto},
        "geoinfo.geometry": {"$exists": True},
        "shape": {"$ne": ""}}, SUMMARY_KEYS)

    stats = {"all": cnt, "complete": cursor.count(), "shapes_count": shapes_count(dfrom, dto)}

    if stats_only:
        return stats
    else:
        return {"results": list(cursor), "stats": stats}


def shapes_count(dfrom, dto):
    """
    return the number of records per shape
    :param dfrom: date from to select records (datetime object)
    :param dto: date to to select records (datetime objects)
    :return: a dictionary of <shape>:<count>
    """
    cursor = db.aggregate([
        {"$match": {"date": {"$gte": dfrom, "$lt": dto}, "shape": {"$ne": ""}}},
        {"$group": {"_id": {"shape": "$shape"}, "cnt": {"$sum": 1}}}
    ])
    shape_count = {d["_id"]["shape"]: d["cnt"] for d in list(cursor)}
    return shape_count


def count_by_date():
    """
    :return: a list of {"year": <year>, "cnt": <count>} summarizing the
    number of records by year
    """
    cursor = db.aggregate([
        {"$group": {
            "_id": {"date": {"$year": "$date"}},
            "cnt": {"$sum": 1}
        }},
        {"$sort": {"_id": 1}}
    ])
    results = [{"year": d["_id"]["date"], "cnt": d["cnt"]} for d in list(cursor)]
    return results
