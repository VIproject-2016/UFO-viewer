import json, os

from flask import Flask, render_template, jsonify
from flask import request
from mongo import *

from utils import *

app = Flask(__name__)

jinja_options = app.jinja_options.copy()

jinja_options.update(dict(
    block_start_string='<%',
    block_end_string='%>',
    variable_start_string='%%',
    variable_end_string='%%',
    comment_start_string='<#',
    comment_end_string='#>'
))
app.jinja_options = jinja_options
app.config["TEMPLATES_AUTO_RELOAD"] = True

@app.route("/")
def root():
    return render_template("layout.html", contentTemplate="visualisations.html")


@app.route("/about")
def about():
    return render_template("layout.html", contentTemplate="about.html")

@app.route("/api/summary/")
def get_summary():
    return json.dumps(count_by_date())

@app.route("/api/reports/stats")
def get_reports_stats():
    ok, tup = _get_date_args(request.args.to_dict())
    return tup if not ok else docToJson(reports(tup[0], tup[1], stats_only=True))

@app.route("/api/reports")
def get_reports():
    ok, tup = _get_date_args(request.args.to_dict())
    return tup if not ok else docToJson(reports(tup[0], tup[1], stats_only=False))

@app.route("/api/frequencies")
def get_frequencies():
    args = request.args.to_dict()
    ok, tup = _get_date_args(args)
    if not ok:
        return tup
    shapes = []
    if "shapes" in args:
        shapes = [ s.strip() for s in args["shapes"].split(",") ]
        print(shapes)
    return json.dumps(frequencies(tup[0], tup[1], shapes))


def _get_reports(args, stats_only=False):
    if not "from" in args or not "to" in args:
        return jsonify("error", "missing required from and to parameters"), 400
    dfrom = parseDate(args["from"])
    dto = parseDate(args["to"])
    if dfrom is None or dto is None:
        return jsonify("error", "from and to: invalid format"), 400

    return docToJson(reports(dfrom, dto, stats_only=stats_only))


def _get_date_args(args):
    if not "from" in args or not "to" in args:
        return False, (jsonify("error", "missing required from and to parameters"), 400)
    dfrom = parseDate(args["from"])
    dto = parseDate(args["to"])
    if dfrom is None or dto is None:
        return False, (jsonify("error", "from and to: invalid format"), 400)
    return True, (dfrom, dto)


if __name__ == "__main__":
    app.run(debug=False, threaded=True)
