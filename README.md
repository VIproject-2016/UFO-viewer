# ![U Found One](ufoapp/static/images/favicon.png) U Found One

**U Found One** is an UFO sightings visualization tool

## Installation

- Install [Python 3](https://www.python.org/)
- Install the following python packages : [`pymongo`](https://api.mongodb.com/python/current/installation.html), [`flask`](http://flask.pocoo.org/)
- Install [MongoDB](https://www.mongodb.com)
- Import the database dump in MongoDB using `mongoimport nuforc`
- Run MongoDB

## Usage

In the `ufoapp` directory of the project, run `python main.py`

Access the page with a browser, using your machine's ip and port `5000`. For example, `http://localhost:5000`.

## License

U Found One is distributed under the terms of the [AGPL License](LICENSE).
